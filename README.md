# OpenML dataset: andes_4

https://www.openml.org/d/45212

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Andes Bayesian Network. Sample 4.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-verylarge.html#andes)

- Number of nodes: 223

- Number of arcs: 338

- Number of parameters: 1157

- Average Markov blanket size: 5.61

- Average degree: 3.03

- Maximum in-degree: 6

**Authors**: C. Conati, A. S. Gertner, K. VanLehn, M. J. Druzdzel.

**Please cite**: ([URL](https://link.springer.com/chapter/10.1007/978-3-7091-2670-7_24)): C. Conati, A. S. Gertner, K. VanLehn, M. J. Druzdzel. On-line Student Modeling for Coached Problem Solving Using Bayesian Networks. In Proceedings of the 6th International Conference on User Modeling, pages 231-242. Springer-Verlag, 1997.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45212) of an [OpenML dataset](https://www.openml.org/d/45212). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45212/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45212/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45212/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

